require "lib.slam"
vector = require "lib.hump.vector"
tlfres = require "lib.tlfres"

require "helpers"

CANVAS_WIDTH = 1600
CANVAS_HEIGHT = 1200
LVL_SIZE = 50
TILE_SIZE = 40

mode = "play"
playerX = 1
playerY = 1
playerSpeed = 1
playerDir = "right"
typed = ""
playerHistory = {}
animationIsPlaying = false
animationPaused = false
animationSpeedUp = false
animationStep = 0
secondsSinceLastAnimationStep = 0

questionMarkWasWritten = false

function love.load()
    -- initialize random number generator
    math.randomseed(os.time())
    ---enable repeated typing
    love.keyboard.setKeyRepeat(true)

    -- set up default drawing options
    love.graphics.setBackgroundColor(0, 0, 0)

    -- load assets
    images = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("images")) do
        if filename ~= ".gitkeep" and filename ~= ".DS_Store" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/"..filename)
        end
    end

    sounds = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
        end
    end
    sounds.type:setVolume(0.5)
    sounds.error:setVolume(0.5)

    music = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename, "stream")
            music[filename:sub(1,-5)]:setLooping(true)
        end
    end

    fonts = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("fonts")) do
        if filename ~= ".gitkeep" then
            fonts[filename:sub(1,-5)] = {}
            for fontsize=20,200 do
                fonts[filename:sub(1,-5)][fontsize] = love.graphics.newFont("fonts/"..filename, fontsize)
            end
        end
    end
    love.graphics.setFont(fonts.specialelite[40])

    animations = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("animations")) do
        if string.match(filename, ".txt$") then
            table.insert(animations, loadAnimation("animations/"..filename))
        end
    end

    levels = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("levels")) do
        if string.match(filename, ".txt$") then
            table.insert(levels, loadLevel("levels/"..filename))
        end
    end

    --list of levels for use in menu:
    levelList = {}
    for i=1,#levels do
        table.insert(levelList, {levels[i].displayName, current = false, chosen = false, solved = false})
    end

    levelnr = 1
    changeLevel(0)
    level = levels[levelnr]

    --music.dark_child:setVolume(0.3)
    music.darkest_child:setVolume(0.3)
    music.darkest_child:play() 
end

function love.update(dt)
    if mode == "menu" then
        --do nothing
    elseif mode == "play"then
        if animationSpeedUp then
            qPause = 0
            stepPause = 0
        else
            qPause = 0.8
            stepPause = 0.03 + 0.1*math.random()
        end
        if animationIsPlaying then
        secondsSinceLastAnimationStep = secondsSinceLastAnimationStep + dt
            if (questionMarkWasWritten and secondsSinceLastAnimationStep >= qPause) then
                questionMarkWasWritten = false
                updateAnimation(level.animation)
                secondsSinceLastAnimationStep = 0
            elseif (questionMarkWasWritten == false and secondsSinceLastAnimationStep >= stepPause )then
                updateAnimation(level.animation)
                secondsSinceLastAnimationStep = 0
            end
        end
    end
end

function love.mouse.getPosition()
    return tlfres.getMousePosition(CANVAS_WIDTH, CANVAS_HEIGHT)
end

function love.keypressed(key)
    if key == "+" then
        vol = music.darkest_child:getVolume()
        if (vol <= 0.9) then
            music.darkest_child:setVolume(vol+0.1)
            --print("music volume set to " .. music.darkest_child:getVolume())
        end
    elseif key == "-" then
        vol = music.darkest_child:getVolume()
        if (vol >= 0.1) then
            music.darkest_child:setVolume(vol-0.1)
            --print("music volume set to " .. music.darkest_child:getVolume())
        elseif (vol > 0 and vol < 0.1) then
            music.darkest_child:setVolume(0)
            --print("music volume set to " .. music.darkest_child:getVolume())
        end
    elseif animationIsPlaying == false then
        pressKey(key)
    elseif (animationIsPlaying and (key == "pagedown" or key == "pageup")) then
        animationIsPlaying = false
        pressKey(key)
    elseif (animationIsPlaying and key == "escape") then
        animationIsPlaying = false
        animationPaused = true
        pressKey(key)
    elseif (animationIsPlaying and key == "return") then
        animationSpeedUp = true
        --print("return was pressed")
    elseif (animationIsPlaying and (key == "space" or key == "")) then
        animationSpeedUp = true
        --print("space was pressed")
    end
end

function pressKey(key)
    if mode == "play" then

        --define all keys for play mode:

        if key == "escape" then
            mode = "menu"
            levelList[levelnr].chosen = true
        elseif key == "backspace" then
            sounds.erase:setPitch(0.95 + 0.1*math.random())
            sounds.erase:play()
            undo()
        elseif key == "pageup" then
            changeLevel(-1)
        elseif key == "pagedown" then
            changeLevel(1)
        elseif key == "f5" then
            sounds.ding:play()
            saveLevel("/tmp/writespace-"..level.name.."-"..math.random(1,100000000)..".txt")
        end

        if animationIsPlaying then
            letters = level.animation.letters
            if string.match(key, "^[a-zA-Z]$") then
                key = string.upper(key)
                typingPlayer(key)
            elseif key == "space" then
                typingPlayer(" ")
            elseif key == "0" then
                sounds.error:play()
            elseif string.match(key, "^[!$%.,+&?'<3]$") then
                typingPlayer(key)
                if key == "?" then
                    questionMarkWasWritten = true
                end
            elseif (key == "return" and animationStep < #letters) then
                playerX = 1
                playerY = playerY + 1
                playerDir = "right"
                if not animationSpeedUp then
                    sounds.ding:play()
                    love.timer.sleep(0.5)
                end
            end
        else
            if string.match(key, "^[a-zA-Z]$") then
                key = string.upper(key)
                typingPlayer(key)
            end
        end

    elseif mode == "menu"then

        --play typing sound:
        sounds.type:play()

        --find out chosen level from levelList:
        chosenlvl = 0
        for i=1,#levelList do
            if levelList[i].chosen then
                chosenlvl = i
            end
        end

        --define all keys for menu mode:
        if (key == "q") then
            love.window.setFullscreen(false)
            love.event.quit()
        elseif (key == "return" and chosenlvl == levelnr) or (key == "escape") then
            mode = "play"
            --resume current animation if it was paused by opening the menu:
            if animationPaused then
                animationPaused = false
                animationIsPlaying = true
            end
        elseif (key == "return" and chosenlvl ~= levelnr) then
            --stop animation from previous level:
            animationIsPlaying = false
            if animationPaused then
                animationPaused = false
            end
            levelList[levelnr].current = false
            levelList[chosenlvl].current = true
            mode = "play"
            goToLevel(chosenlvl)
        elseif (key == "up" and chosenlvl > 1) then
            levelList[chosenlvl].chosen = false
            chosenlvl = chosenlvl - 1
            levelList[chosenlvl].chosen = true
        elseif (key == "down" and chosenlvl < #levelList) then
            levelList[chosenlvl].chosen = false
            chosenlvl = chosenlvl + 1
            levelList[chosenlvl].chosen = true
        end
    end 
    --end of key definitions
end


function typingPlayer(key)
    if (level.tiles[playerX][playerY] == " " or level.tiles[playerX][playerY] == "*" or level.tiles[playerX][playerY] == "@") and (level.letters[playerX][playerY].letter == key or level.letters[playerX][playerY].letter == " ") then

        --play sound and add letter to level:

        typed = typed .. key
        sounds.type:setPitch(0.95 + 0.1*(string.byte(key)-65)/26)
        sounds.type:play()
        level.letters[playerX][playerY].letter = key
        level.letters[playerX][playerY].count = level.letters[playerX][playerY].count + 1

        if level.tiles[playerX][playerY] == "*" then
            sounds.pickup:play()
        end

        table.insert(playerHistory, {playerX, playerY, playerDir})

        --check if a command was typed:
        if string.match(typed, "DOWN$") then
            playerDir = "down"
        elseif string.match(typed, "UP$") then
            playerDir = "up"
        elseif string.match(typed, "LEFT$") then
            playerDir = "left"
        elseif string.match(typed, "RIGHT$") then
            playerDir = "right"
        --elseif string.match(typed, "JUMP$") then
        --    playerSpeed = 2
        end

        if playerDir == "right" then
            playerX = playerX+playerSpeed
        elseif playerDir == "left" then
            playerX = playerX-playerSpeed
        elseif playerDir == "up" then
            playerY = playerY-playerSpeed
        elseif playerDir == "down" then
            playerY = playerY+playerSpeed
        end

        if playerX < 1 or playerY < 1 or playerX > LVL_SIZE or playerY > LVL_SIZE then
            sounds.error:play()
            undo()
        end

        playerSpeed = 1

        if level.tiles[playerX][playerY] == ">" and itemsCollected() then
            sounds.carriage:play()
            levelList[levelnr].solved = true
            changeLevel(1)
        end

    else
        sounds.error:play()
    end
end

function love.keyreleased(key)
    if (animationIsPlaying and key == "return") then
        animationSpeedUp = false
    elseif (animationIsPlaying and key == "space") then
        animationSpeedUp = false
    end
end

function love.mousepressed(x, y, button)
end

function love.draw()
    love.graphics.setColor(1, 1, 1, 1)
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)
	
    if mode == "menu" then
        --background image for menu:
        love.graphics.draw(images.default,0,0,0,CANVAS_HEIGHT/images.default:getHeight(),CANVAS_WIDTH/images.default:getWidth())
        
        --menu headline:
        love.graphics.setColor(0, 0, 0, 1)
        love.graphics.setFont(fonts.specialelite[80])
        header = "INDEX"
        love.graphics.printf(header, 0, 2.5*TILE_SIZE, CANVAS_WIDTH, "center")

        --display the list of levels with markers for solved and current:
        love.graphics.setFont(fonts.specialelite[40])
        col1x = 9*TILE_SIZE
        col2x = CANVAS_WIDTH/2 + 1*TILE_SIZE
            --calculate an even number for the menu entries
        if #levelList % 2 == 0 then
            lvls = #levelList
        else
            lvls = #levelList+1
        end
        for i=1,#levelList do
            --set position:
            if i <= (lvls/2) then
                posX = col1x
                posY = 4.5*TILE_SIZE + 2.5*i*TILE_SIZE
            elseif i <= lvls then
                posX = col2x
                posY = 4.5*TILE_SIZE + 2.5*(i-lvls/2)*TILE_SIZE
            end
            --set color:
            if levelList[i].current then
                color = "blue"
            elseif levelList[i].solved then
                color = "grey"
            else
                color = "black"
            end
            --print the level name:
            writeLetter(posX/TILE_SIZE, posY/TILE_SIZE, i .. ". " .. levelList[i][1], color)
            
            --set the cursor position:
            love.graphics.setColor(1, 1, 1, 1)
            if levelList[i].chosen then
                guideX = posX-18
                if i > 9 then
                    guideX = guideX + 0.3 * TILE_SIZE
                end
                guideY = posY-16
            end
        end

        --menu footer:
        love.graphics.setColor(0, 0, 0, 0.5)
        footer = "ENTER: play level | +/-: music volume | Q: quit"
        love.graphics.printf(footer, 0, CANVAS_HEIGHT-(2*TILE_SIZE), CANVAS_WIDTH, "center")

        --draw the paper structure:
        love.graphics.setColor(1.0, 1.0, 1.0, 0.6)

        love.graphics.draw(images.structure,0,0,0,CANVAS_HEIGHT/images.structure:getHeight(),CANVAS_WIDTH/images.structure:getWidth())
        --draw the cursor:
        love.graphics.draw(images.guide, guideX, guideY, 0, 0.6, 0.6)

    elseif mode == "play" then
        --draw commands for play mode:

        if images[level.name] ~= NIL then
	        love.graphics.draw(images[level.name],0,0,0, CANVAS_HEIGHT/images[level.name]:getHeight(), CANVAS_WIDTH/images[level.name]:getWidth())
	    else
	        love.graphics.draw(images.default,0,0,0,CANVAS_HEIGHT/images.default:getHeight(),CANVAS_WIDTH/images.default:getWidth())
        end
        love.graphics.setColor(1.0, 1.0, 1.0, 1.0) 
        love.graphics.draw(images.corner,0,0,0,CANVAS_HEIGHT/images.corner:getHeight(),CANVAS_WIDTH/images.corner:getWidth())
	    
	    love.graphics.setColor(0, 0, 0, 1)

        --here comes the logo for the title level
        if level.name == "title" then
            love.graphics.setFont(fonts.specialelite[200])
            love.graphics.setColor(0, 0, 0, 0.45)
            logo2 = "WHITESPACE"
            love.graphics.printf (logo2, 12, 4.3*TILE_SIZE+12, CANVAS_WIDTH, "center")

            love.graphics.setFont(fonts.specialelite[200])
            love.graphics.setColor(0, 0, 0, 1)
            logo1 = "WRITESPACE"
            love.graphics.printf (logo1, 0, 4.3*TILE_SIZE, CANVAS_WIDTH, "center")

        end
        --end of logo, next are the normal letters:
        
        love.graphics.setColor(0, 0, 0, 1)
        love.graphics.setFont(fonts.specialelite[40])

        math.randomseed(42)
        for x=1,LVL_SIZE do
            for y=1,LVL_SIZE do
                tile = level.tiles[x][y]
                if tile == "#" then
                    local characters = "#%&"
                    local r = math.random(#characters)
                    local c = characters:sub(r,r)
                    writeLetter(x,y,c,"black")
                elseif tile == ">" then
                    if itemsCollected() then
                        writeLetter(x,y,">", "blue")
                    else
                        writeLetter(x,y,">", "black")
                    end
                elseif tile == "*" then
		            if level.letters[x][y].letter == " " then
                        writeLetter(x,y,"*","blue")
		            end
                elseif tile ~= "@" then
                    writeLetter(x,y,tile,"black")
                end
            end
        end
        math.randomseed(os.time())
    
        setHighlights()

        for x=1,LVL_SIZE do
            for y=1,LVL_SIZE do
                letter = level.letters[x][y].letter
                if level.letters[x][y].highlighted then
                    writeLetter(x, y, letter, "blue")
                    eraseHighlight(x, y)
                else
                    writeLetter(x, y, letter, "black")
                end
            end
        end

        love.graphics.setColor(0, 0, 0, 0.5)
        love.graphics.printf(level.displayName.." | ESC: menu", CANVAS_WIDTH/2, CANVAS_HEIGHT-2*TILE_SIZE, CANVAS_WIDTH/2-2*TILE_SIZE, "right")

        love.graphics.setColor(1.0, 1.0, 1.0, 0.7)
        love.graphics.draw(images.structure,0,0,0,CANVAS_HEIGHT/images.structure:getHeight(),CANVAS_WIDTH/images.structure:getWidth())

	    love.graphics.setColor(255, 255, 255, 255)
        love.graphics.draw(images.guide, playerX*TILE_SIZE-18, playerY*TILE_SIZE-16, 0, 0.6, 0.6)

    end --end of draw commands for play mode

    tlfres.endRendering()
end

function writeLetter(x, y, letter, color)
    if color == "blue" then
        phase1 = ( x + y)*0.2 + love.timer.getTime() * 2
        phase2 = (-x + y)*0.2 + love.timer.getTime() * 1.7
        alpha = math.max(0, (math.sin(phase1) + math.sin(phase2)) * 0.8 - 1.20)
        for dx=-3,4 do
            for dy=-3,4 do
                if (dx*dx + dy*dy) > 5 then
                    love.graphics.setColor(0.8, 0.8, 1.0, alpha * 0.4)
                else
                    love.graphics.setColor(1.0, 1.0, 1.0, alpha * 0.8)
                end
                love.graphics.print(letter, x*TILE_SIZE+dx, y*TILE_SIZE+dy)
            end
        end
        love.graphics.setColor(0.15 + alpha * 1.6, 0.15 + alpha * 1.6, 1.0, 1.0)
        -- draw the letter now, offset by one pixel, and then again outside the if-block
        love.graphics.print(letter, x*TILE_SIZE+1, y*TILE_SIZE)
    elseif color == "black" then 
        love.graphics.setColor(0, 0, 0, 1)
    elseif color == "grey" then 
        love.graphics.setColor(0, 0, 0, 0.5)
    else
        love.graphics.setColor(1, 0, 0, 1)
    end
    love.graphics.print(letter, x*TILE_SIZE, y*TILE_SIZE)
end

function startAnimation(animation)
    --print("animation was started")
    animationIsPlaying = true
    letters = animation.letters
    animationStep = 0
    updateAnimation(animation)
end

function stopAnimation()
    for i=1,#playerHistory do
        playerHistory[i].locked = true
        --print("letter number " .. i .. " has been locked")
    end
    animationIsPlaying = false
    animationStep = 0
    animationSpeedUp = false
end

function updateAnimation(animation)
    --print("updateAnimation was called")
    animationStep = animationStep+1
    s = animationStep
    if s == #letters then
        stopAnimation()
    elseif animation.letters[s] == " " then
        pressKey("space")
    elseif animation.letters[s] == "\n" then
        pressKey("return")
    else
        pressKey(animation.letters[s])
    end
end


function loadAnimation(filename)
    animation = {filename=filename,name=string.sub(filename,12,#filename-4), letters={}}

    local f = love.filesystem.newFile(filename)
    f:open("r")

    text = f:read()

    for i=1,#text do
        letter = text:sub(i,i)
        animation.letters[i] = letter
    end

    return animation
end


function loadLevel(filename)
    level = {filename=filename, name=string.sub(filename,11,#filename-4), tiles={}, letters={}, animated=false, animation=nil}
    level.displayName = level.name:gsub("_", " ")
    for x=1,LVL_SIZE do
        level.tiles[x] = {}
        for y=1,LVL_SIZE do
            level.tiles[x][y] = " "
        end
    end
    for x=1,LVL_SIZE do
        level.letters[x] = {}
        for y=1,LVL_SIZE do
            level.letters[x][y] = {letter=" ", count=0, highlighted=false}
        end
    end

    local f = love.filesystem.newFile(filename)
    f:open("r")

    y = 1
    for line in f:lines() do
        for x=1,#line do
            letter = line:sub(x,x)
            if string.match(letter, "[a-zA-Z]") then
                level.letters[x][y].letter = letter
                level.letters[x][y].count = 1
            else
                level.tiles[x][y] = letter
                if letter == "@" then
                    playerX = x
                    playerY = y
                    --table.insert(playerHistory, {x, y, playerDir, locked=false})
                end
            end
        end
        y = y+1
    end

    for i=1,#animations do
        --print(animations[i].name)
        if animations[i].name == level.name then
            --print("level" .. level.name .. "is animated")
            level.animated = true
            level.animation = animations[i]
        end
    end

    return level
end

function saveLevel(filename)
    local f = io.open(filename, "w")

    for y=1,LVL_SIZE do
        for x=1,LVL_SIZE do
            letter = level.letters[x][y].letter
            if letter == " " then
                letter = level.tiles[x][y]
            end
            f:write(letter)
        end
        f:write("\n")
    end
    f:close()
end

function goToLevel(nr)
    diff = nr - levelnr
    changeLevel(diff)
end

function changeLevel(diff)
    levelList[levelnr].current = false
    levelList[levelnr].chosen = false
    levelnr = 1 + ((levelnr-1) + diff) % #levels
    levelList[levelnr].current = true
    levelList[levelnr].chosen = true
    level = levels[levelnr]
    level = loadLevel(level.filename)
    playerHistory = {}
    playerDir = "right"
    typed = ""
    if level.animated then
        animationStep = 0
        startAnimation(level.animation)
    end
end

function setHighlights()
        written = ""
    for i=1,#playerHistory do
        written = written .. level.letters[playerHistory[i][1]][playerHistory[i][2]].letter
        if string.match(written:sub(1,i), "DOWN$") then
            highlight(4, i)
        elseif string.match(written:sub(1,i), "UP$") then
            highlight(2, i)
        elseif string.match(written:sub(1,i), "LEFT$") then
            highlight(4, i)
        elseif string.match(written:sub(1,i), "RIGHT$") then
            highlight(5, i)
		elseif string.match(written:sub(1,i), "<3$") then
            highlight(2, i)
        --elseif string.match(written:sub(1,i), "JUMP$") then
        --    highlight(4, i)
        end
    end
end

function eraseHighlight(xPos,yPos)
    level.letters[xPos][yPos].highlighted = false
end

function highlight(length, endPos)
    for l=0,length-1 do
        xPos = playerHistory[endPos-l][1]
        yPos = playerHistory[endPos-l][2]
        level.letters[xPos][yPos].highlighted = true
    end
end

function undo()
    if (#playerHistory > 0 and not playerHistory[#playerHistory].locked) then
        oldPos = table.remove(playerHistory)
        playerX = oldPos[1]
        playerY = oldPos[2]
        playerDir = oldPos[3]
        level.letters[playerX][playerY].count = level.letters[playerX][playerY].count - 1
        if level.letters[playerX][playerY].count == 0 then
            level.letters[playerX][playerY].letter = " "
        end
        typed = typed:sub(1, #typed-1)


    end
end

function itemsCollected()
    for y=1,LVL_SIZE do
        for x=1,LVL_SIZE do
            if level.tiles[x][y] == "*" then
                if level.letters[x][y].letter == " " then
                    return false
                end
            end
        end
    end
    return true
end

