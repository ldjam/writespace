Writespace is a puzzle game where you type to move.

Made in 72 hours for the Ludum Dare 42 game jam:
https://ldjam.com/events/ludum-dare/42/writespace

sounds we used in this game:
- https://freesound.org/people/ddohler/sounds/9098/
- https://freesound.org/people/alukahn/sounds/171234/
- https://freesound.org/people/InspectorJ/sounds/411642/
